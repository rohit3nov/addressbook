<?php namespace App\Http\Controllers;
use App\Model\Address;
use Illuminate\Http\Request;

class AdressBookController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/  

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() 
	{
            return view('address_book.index');
	}
	public function create() 
	{
            return view('address_book.add');
	}
	public function store(Request $request) 
	{
            
            echo '<pre>';
            print_r($request->all());
            echo '</pre>';
            exit();
            $rules[] = '';
            $rules['address_book_title'] ='required' ;
            $rules['contact_person_name'] ='required' ;
            $rules['contact_person_number'] ='required' ;
            $rules['address_line_1'] ='required' ;
            $rules['address_line_2'] ='required' ;
            $rules['address_line_3'] ='required' ;
            $rules['pin_code'] ='required' ;
            $rules['city'] ='required' ;
            $rules['state'] ='required' ;
            $rules['country'] ='required' ;
            $messages = [''];
            $validator = \Validator::make($request->all(),$rules);
            if($validator->fails()){
                return redirect()->back()->withInput()->withErrors($validator);
            }

            $address = new Address();
            $address->fill($request->all());
            $addressSave = $address->save();
            if($addressSave){
                return redirect()->route('addressbook.index');
            } else {
                return redirect()->back()->withInput()->withErrors(array('error' => 'Couldn\'t add new address. Please try again'));
            }
	}
        

}
