@extends('app')

@section('content')

  <div class="container">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <form class="form-horizontal" role="form" action="/product/create" method="POST">
                        <div class="form-group">
                            <label for="address_book_title" class="col-sm-2 control-label">Address Book Title</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="address_book_title" placeholder="Address Book Title" name="address_book_title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact_person_name" class="col-sm-2 control-label">Contact Person Name</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="contact_person_name" placeholder="Contact Person Name" name="contact_person_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact_person_name" class="col-sm-2 control-label">Contact Person Number</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="contact_person_number" placeholder="Contact Person Number" name="contact_person_number">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="address_line_1" class="col-sm-2 control-label">Address Line 1</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="address_line_1" placeholder="address line 1" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address_line_2" class="col-sm-2 control-label">Address Line 2</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="address_line_2" placeholder="address line 2" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address_line_3" class="col-sm-2 control-label">Address Line 3</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="address_line_3" placeholder="address line 3" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pin_code" class="col-sm-2 control-label">Pin Code</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="pin_code" placeholder="Pin Code" name="pin_code">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city" class="col-sm-2 control-label">City</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="city" placeholder="City" name="city">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="state" class="col-sm-2 control-label">State</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="state" placeholder="State" name="state">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="country" class="col-sm-2 control-label">Country</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="country" placeholder="Country" name="country">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
