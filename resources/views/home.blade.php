@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-1">
                            <a href="{{route('profile.index')}}" class="btn btn-lg btn-primary">Manage Profile</a>
                            <a href="{{route('addressbook.index')}}" class="btn btn-lg btn-success">Manage Address Book</a>
		</div>
	</div>
</div>
@endsection
